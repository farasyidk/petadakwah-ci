<?php
class Maps_md extends CI_Model {

  public function get_kecamatan(){
      $query = $this->db->get('data_kecamatan');
      return $query->result_array();
  }

  public function get_keagamaan($kcm="") {
      $query = $this->db->get_where('data_keagamaan',['id_kecamatan' => $kcm]);
      return $query->result_array();
  }

  public function get_kabupaten(){
      $query = $this->db->get('districts');
      return $query->result_array();
  }

  public function select_prov($prov) {
      $this->db->like('id_pro',$prov);
      $query = $this->db->get('provinces', 5, 0);
      return $query->result_array();
  }

  public function select_kab($prov, $set="") {
      $this->db->like('id_dis',$prov);
      if ($set != "") {
          $this->db->like('sub',$set);
      }
      $query = $this->db->get('districts', 5, 0);
      return $query->result_array();
  }

  public function select_kec($prov, $set="") {
      $this->db->like('id_kec',$prov);
      if ($set != "") {
          $this->db->like('id_kab',$set);
      }
      $query = $this->db->get('data_kecamatan', 5, 0);
      return $query->result_array();
  }
}
