<?php
class Admin_md extends CI_Model {

    public function get_keagamaan(){
        //$query = $this->db->query('select * from data_keagamaan, sub-district, districts, provinces where data_keagamaan.id_kecamatan = sub-district.id and sub-district.sub = districts.id and districts.sub = provinces.id')->get();
        // print_r($kcm." kc ".$kbp." kb ".$prv);
        $this->db->select('*');
        $this->db->from('data_keagamaan');
        $this->db->join('data_kecamatan', 'data_kecamatan.id_kec = data_keagamaan.id_kecamatan');
        $this->db->join('districts', 'districts.id_dis = data_kecamatan.id_kec');
        $this->db->join('provinces', 'provinces.id_pro = districts.sub');
        switch ($this->session->userdata('akses')) {
            case 1: $this->db->where('data_kecamatan.id_kec', $this->session->userdata('ses_idDaerah')); break;
            case 2: $this->db->where('districts.id_dis', $this->session->userdata('ses_idDaerah')); break;
            case 3: $this->db->where('provinces.id_pro', $this->session->userdata('ses_idDaerah')); break;
            default: break;
        }
        $this->db->order_by('nama_kelurahan', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_problem(){
        $this->db->select('*');
        $this->db->from('problem_keagamaan');
        $this->db->join('data_kecamatan', 'data_kecamatan.id_kec = problem_keagamaan.id_kecamatan');
        $this->db->join('districts', 'districts.id_dis = data_kecamatan.id_kec');
        $this->db->join('provinces', 'provinces.id_pro = districts.sub');
        switch ($this->session->userdata('akses')) {
            case 1: $this->db->where('data_kecamatan.id_kec', $this->session->userdata('ses_idDaerah')); break;
            case 2: $this->db->where('districts.id_dis', $this->session->userdata('ses_idDaerah')); break;
            case 3: $this->db->where('provinces.id_pro', $this->session->userdata('ses_idDaerah')); break;
            default: break;
        }
        $this->db->order_by('tanggal', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }
     public function get_organisasi(){
        $this->db->select('*');
        $this->db->from('organisasi_keagamaan');
        $this->db->join('data_kecamatan', 'data_kecamatan.id_kec = organisasi_keagamaan.id_kecamatan');
        $this->db->join('districts', 'districts.id_dis = data_kecamatan.id_kec');
        $this->db->join('provinces', 'provinces.id_pro = districts.sub');
        switch ($this->session->userdata('akses')) {
            case 1: $this->db->where('data_kecamatan.id_kec', $this->session->userdata('ses_idDaerah')); break;
            case 2: $this->db->where('districts.id_dis', $this->session->userdata('ses_idDaerah')); break;
            case 3: $this->db->where('provinces.id_pro', $this->session->userdata('ses_idDaerah')); break;
            default: break;
        }
        $this->db->order_by('nama_organisasi', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_edit_problem($id){
        $this->db->select('*');
        $this->db->from('problem_keagamaan');
        $this->db->where('id', $id);
        $this->db->order_by('tanggal', 'DESC');
        $query = $this->db->get();
        return $query->row();
    }
    public function get_edit_keagamaan($id){
        //$query = $this->db->query('select * from data_keagamaan, sub-district, districts, provinces where data_keagamaan.id_kecamatan = sub-district.id and sub-district.sub = districts.id and districts.sub = provinces.id')->get();
        $this->db->select('*');
        $this->db->from('data_keagamaan');
        $this->db->join('sub-district', 'sub-district.id_sub = data_keagamaan.id_kecamatan');
        $this->db->join('districts', 'districts.id_dis = sub-district.sub');
        $this->db->join('provinces', 'provinces.id_pro = districts.sub');
        $this->db->where('id_agama', $id);
        $this->db->order_by('nama_kelurahan', 'ASC');
        $query = $this->db->get();
        return $query->row();
    }
    public function getDaerah($tbl, $id) {
        return $this->db->get_where($tbl, 'id');
    }
    public function m_hdata($id) {
        $this->db->where('id_agama', $id);
        if($this->db->delete('data_keagamaan') == true){
            return true;
        }else{
            return false;
        }
    }
    public function m_tdata() {
        $data = [
            'nama_kelurahan' => $this->input->post('kelurahan'),
            'jumlah_penduduk' =>$this->input->post('penduduk'),
            'islam'=>$this->input->post('islam'),
            'kristen_protestan'=>$this->input->post('protestan'),
            'kristen_katolik'=>$this->input->post('katolik'),
            'hindu'=>$this->input->post('hindu'),
            'budha'=>$this->input->post('budha'),
            'konghucu'=>$this->input->post('konghucu'),
            'jml_masjid'=>$this->input->post('masjid'),
            'pesantren'=>$this->input->post('pesantren'),
            'jml_dai'=>$this->input->post('dai'),
            'gereja'=>$this->input->post('gereja'),
            'pura'=>$this->input->post('pura'),
            'kelenteng'=>$this->input->post('kelenteng'),
            'tahun'=>$this->input->post('tahun'),
            'keterangan'=>$this->input->post('keterangan'),
            'id_kecamatan'=>$this->session->userdata('ses_idDaerah'),
        ];
        if($this->db->insert('data_keagamaan',$data)){
            return true;
        }else{
            return false;
        }
    }
     public function m_edata() {
        $data = [
            'nama_kelurahan' => $this->input->post('kelurahan'),
            'jumlah_penduduk' =>$this->input->post('penduduk'),
            'islam'=>$this->input->post('islam'),
            'kristen_protestan'=>$this->input->post('protestan'),
            'kristen_katolik'=>$this->input->post('katolik'),
            'hindu'=>$this->input->post('hindu'),
            'budha'=>$this->input->post('budha'),
            'konghucu'=>$this->input->post('konghucu'),
            'jml_masjid'=>$this->input->post('masjid'),
            'pesantren'=>$this->input->post('pesantren'),
            'jml_dai'=>$this->input->post('dai'),
            'gereja'=>$this->input->post('gereja'),
            'pura'=>$this->input->post('pura'),
            'kelenteng'=>$this->input->post('kelenteng'),
            'tahun'=>$this->input->post('tahun'),
            'keterangan'=>$this->input->post('keterangan'),
            'id_kecamatan'=>$this->session->userdata('ses_idDaerah'),
        ];
        $this->db->where('id_agama', $this->input->post('id'));
        if($this->db->update('data_keagamaan',$data)){
            return true;
        }else{
            return false;
        }
    }
    public function m_tproblem() {
        $data = [
            'tanggal' => $this->input->post('tanggal'),
            'nama_problem' =>$this->input->post('masalah'),
            'uraian_problem'=>$this->input->post('uraian'),
            'lokasi_problem'=>$this->input->post('lokasi'),
            'longitude'=>$this->input->post('lat'),
            'latitude'=>$this->input->post('lng'),
            'id_kelurahan'=>$this->session->userdata('ses_idDaerah')
        ];
        if($this->db->insert('problem_keagamaan',$data)){
            return true;
        }else{
            return false;
        }
    }
     public function m_eproblem() {
        $data = [
            'tanggal' => $this->input->post('tanggal'),
            'nama_problem' =>$this->input->post('masalah'),
            'uraian_problem'=>$this->input->post('uraian'),
            'lokasi_problem'=>$this->input->post('lokasi'),
            'longitude'=>$this->input->post('lat'),
            'latitude'=>$this->input->post('lng'),
            'id_kelurahan'=>$this->session->userdata('ses_idDaerah')
        ];
        $this->db->where('id', $this->input->post('id'));
        if($this->db->update('problem_keagamaan',$data)){
            return true;
        }else{
            return false;
        }
    }
}
